const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

  let myvar = undefined;
  before(() => {
    myvar = 1;
    console.log('Begin "mylib" testing')
  })

  beforeEach(() => {
    console.log('Test starting')
  })

  afterEach(() => {
    console.log('Test finished')
  })

  // Älä kommentoi testejä pois päältä, käytä mieluummin "it.skip()" -funktiota.
  it.skip('Assert foo is not bar', () => {
    assert('foo' !== 'bar');
  })

  it('Myvar should exist', () => {
    should.exist(myvar)
  })

  it('Sum should return 2 when using sum function with a=myvar, b=1', () => {
    const result = mylib.sum(myvar, 1);
    expect(result).to.equal(2);
    console.log("sum =",result)
  })

  it('Random should generate integer within range 0...99', () => {
    const result = mylib.random();
    console.log(result)
    expect(result).to.greaterThanOrEqual(0)
    expect(result).to.lessThanOrEqual(99)
  })

  after(() => {
    console.log('End "mylib" testing')
  })
});